import FONT_COLOR from '../config.js'

export default () => {
  return (
    <div>
      <span>hello</span>
      <style jsx>{`
        color: ${FONT_COLOR};
      `}</style>
    </div>
  )
}
